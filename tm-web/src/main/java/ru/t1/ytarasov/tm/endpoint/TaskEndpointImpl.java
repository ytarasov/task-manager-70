package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint;
import ru.t1.ytarasov.tm.api.service.ITaskDtoService;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.service.TaskDtoService;
import ru.t1.ytarasov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/endpoint/tasks")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @Override
    @WebMethod
    @SneakyThrows
    @PutMapping("/createWithUserId")
    public void create() {
        taskDtoService.createWithUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/saveWithUserId")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody
            final
            TaskDto task
    ) {
        taskDtoService.saveWithUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskDtoService.findByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return taskDtoService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/count")
    public long count() {
        return taskDtoService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return taskDtoService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody
            final
            TaskDto task
    ) {
        taskDtoService.deleteWithUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        taskDtoService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody
            final
            List<TaskDto> tasks) {
        taskDtoService.deleteAllWithUserId(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @DeleteMapping("/clear")
    public void clear() {
        taskDtoService.deleteByUserId(UserUtil.getUserId());
    }

}