package ru.t1.ytarasov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "yyyy-MM-dd";

    @NotNull SimpleDateFormat FORMAT = new SimpleDateFormat(PATTERN);

    @SneakyThrows
    static Date toDate(@NotNull final String value) {
        return "".equals(value) ? null : FORMAT.parse(value);
    }

    static String toString(@Nullable final Date value) {
        if (value == null) return null;
        return FORMAT.format(value);
    }

}
