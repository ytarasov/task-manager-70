package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class TaskDto {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String description;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @Column(name = "date_start")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Column(name = "date_finish")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateFinish;

    @Column(name = "user_id")
    private String userId;

    @Nullable
    private String projectId;

    public TaskDto(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
