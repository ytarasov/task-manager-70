package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDbUrl();

    @NotNull
    String getDbUsername();

    @NotNull
    String getDbPassword();

    @NotNull
    String getDbDriver();

    @NotNull
    String getDbDialect();

    @NotNull
    String getDbHbm2ddlAuto();

    @NotNull
    String getShowSql();

    @NotNull
    String getFormatSql();

    @NotNull
    String getSecondLvlCash();

    @NotNull
    String getFactoryClass();

    @NotNull
    String getUseQueryCash();

    @NotNull
    String getUseMinPuts();

    @NotNull
    String getRegionPrefix();

    @NotNull
    String getConfigFilePath();

}
