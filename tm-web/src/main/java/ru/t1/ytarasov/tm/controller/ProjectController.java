package ru.t1.ytarasov.tm.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.service.ProjectDtoService;
import ru.t1.ytarasov.tm.util.UserUtil;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDtoService projectDtoService;

    @SneakyThrows
    @PostMapping("/project/create")
    public String create() {
        projectDtoService.createWithUserId(UserUtil.getUserId());
        return "redirect:/projects";
    }

    @SneakyThrows
    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectDtoService.deleteByUserIdAndId(UserUtil.getUserId(), id);
        return "redirect:/projects";
    }

    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final ProjectDto project = projectDtoService.findByUserIdAndId(UserUtil.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @SneakyThrows
    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute ProjectDto project, BindingResult result) {
        projectDtoService.saveWithUserId(UserUtil.getUserId(), project);
        return "redirect:/projects";
    }

}
