package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.service.ProjectDtoService;
import ru.t1.ytarasov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @Override
    @WebMethod
    @SneakyThrows
    @PutMapping("/createWithUserId")
    public void create() {
        projectDtoService.createWithUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/saveWithUserId")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project
    ) {
        projectDtoService.saveWithUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectDtoService.findByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return projectDtoService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/count")
    public long count() {
        return projectDtoService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return projectDtoService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project) {
        projectDtoService.deleteWithUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        projectDtoService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody
            final
            List<ProjectDto> projects) {
        projectDtoService.deleteAllWithUserId(UserUtil.getUserId(), projects);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @DeleteMapping("/clear")
    public void clear() {
        projectDtoService.deleteByUserId(UserUtil.getUserId());
    }

}
