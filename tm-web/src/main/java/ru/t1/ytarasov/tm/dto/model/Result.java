package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public Result(@NotNull Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        this.success = false;
        this.message = e.getMessage();
    }

}
