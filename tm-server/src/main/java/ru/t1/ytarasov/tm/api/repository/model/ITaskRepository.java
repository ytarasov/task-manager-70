package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT t FROM Task t ORDER BY :sortOrder")
    List<Task> findAllWithSort(@NotNull @Param("sortOrder") final String sortOrder);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sortOrder")
    List<Task> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortOrder") final String sortOrder
    );

    @Nullable
    List<Task> findByUserId(@NotNull final String userId);

    @Nullable
    Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String userId);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<Task> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
