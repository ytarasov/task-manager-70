package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY :sortType")
    List<SessionDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.userId = :userId ORDER BY :sortType")
    List<SessionDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    List<SessionDTO> findByUserId(@NotNull final String userId);

    @Nullable
    SessionDTO findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String id);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
